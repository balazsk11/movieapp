﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.DataTransferObjects.Base
{
    /// <summary>
    /// base class for data transfer objects
    /// </summary>
    public class DataTransferObjectBase
    {
        /// <summary>
        /// The heading that appears in a list view
        /// </summary>
        public string Heading { get; private set; }

        /// <summary>
        /// The picture that appears in a list view
        /// </summary>
        public string PictureUri { get; private set; }

        /// <summary>
        /// Creates an instance of DataTransferObject
        /// </summary>
        public DataTransferObjectBase(string heading, string pictureUri)
        {
            Heading = heading;
            PictureUri = pictureUri;
        }
    }
}