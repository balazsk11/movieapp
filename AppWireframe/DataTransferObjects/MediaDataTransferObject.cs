﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.DataTransferObjects
{
    using AppWireframe.DataTransferObjects.Base;
    using System;

    /// <summary>
    /// Data transfer class for movies and tv shows
    /// </summary>
    public class MediaDataTransferObject : DataTransferObjectBase
    {
        /// <summary>
        /// Short description of a media
        /// </summary>
        public string Overview { get; private set; }

        /// <summary>
        /// Release date of the movie
        /// </summary>
        public DateTime ReleaseDate { get; private set; }

        /// <summary>
        /// The popularity of a media 
        /// </summary>
        public double Rating { get; private set; }

        /// <summary>
        /// Creates an instance of a media
        /// No other constructors are available
        /// </summary>
        /// <param name="title">The heading that appears in a list view</param>
        /// <param name="pictureUri">The picture that appears in a list view</param>
        /// <param name="overview">Short description of a media</param>
        /// <param name="releaseDate">Release date of the movie</param>
        /// <param name="rating">The popularity of a media </param>
        public MediaDataTransferObject(
            string title,
            string pictureUri,
            string overview,
            DateTime releaseDate,
            double rating) 
            : base(title, pictureUri)
        {
            Overview = overview;
            ReleaseDate = releaseDate;
            Rating = rating;
        }
    }
}