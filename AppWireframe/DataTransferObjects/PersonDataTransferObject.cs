﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.DataTransferObjects
{
    using AppWireframe.DataTransferObjects.Base;
    using System.Collections.Generic;

    /// <summary>
    /// Data transfer class for persons
    /// </summary>
    public class PersonDataTransferObject : DataTransferObjectBase
    {
        /// <summary>
        /// The popularity of an actor or actress
        /// </summary>
        public double Popularity { get; private set; }

        /// <summary>
        /// Most known movies
        /// </summary>
        public List<MediaDataTransferObject> Filmography { get; private set; }

        /// <summary>
        /// Creates an instance of person data tranfer object
        /// No other constructors are available
        /// </summary>
        /// <param name="name"></param>
        /// <param name="pictureUri"></param>
        /// <param name="popularity"></param>
        /// <param name="filmography"></param>
        public PersonDataTransferObject(
            string name,
            string pictureUri,
            double popularity,
            List<MediaDataTransferObject> filmography)
            :base(name, pictureUri)
        {
            Popularity = popularity;
            Filmography = filmography;
        }
    }
}
