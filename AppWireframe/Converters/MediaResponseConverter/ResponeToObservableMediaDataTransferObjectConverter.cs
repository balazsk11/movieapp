﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Converters.MediaResponseConverter
{
    using AppWireframe.Beans;
    using AppWireframe.Converters.PicturePath;
    using AppWireframe.DataTransferObjects;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Use this static class to convert a list of 
    /// <c>MovieInfo</c>, <c>TvShowInfo</c> or <c>PersonInfo</c>int Data transfer objects
    /// </summary>
    public static class ResponeToObservableMediaDataTransferObjectConverter
    {
        /// <summary>
        /// Convers list of <c>MovieInfo</c> into a collection of <c>MediaDataTransferObject</c>
        /// </summary>
        public static ObservableCollection<MediaDataTransferObject>
            ConvertMoviesToDataTransferCollection(List<MovieInfo> data)
        {
            try
            {
                ObservableCollection<MediaDataTransferObject> resultCollection =
                    new ObservableCollection<MediaDataTransferObject>();

                foreach (MovieInfo currentMedia in data)
                {
                    MediaDataTransferObject media = new MediaDataTransferObject(
                                                                        currentMedia.OriginalTitle,
                                                                        PicturePathConverter
                                                                            .GetPictureFullPath(
                                                                                        currentMedia.PosterPath),
                                                                        currentMedia.Overview,
                                                                        currentMedia.ReleaseDate,
                                                                        currentMedia.Popularity);
                    resultCollection.Add(media);
                }

                return resultCollection;
            }
            catch (NullReferenceException)
            {
                System.Diagnostics.Debug.WriteLine("Could not parse objects from the server!");

                return new ObservableCollection<MediaDataTransferObject>();
            }
        }

        /// <summary>
        /// Convers list of <c>TvShowInfo</c> into a collection of <c>MediaDataTransferObject</c>
        /// </summary>
        public static ObservableCollection<MediaDataTransferObject>
            ConvertTvShowsToDataTransferCollection(List<TvShowInfo> data)
        {
            try
            {
                ObservableCollection<MediaDataTransferObject> resultCollection =
                    new ObservableCollection<MediaDataTransferObject>();

                foreach (TvShowInfo currentMedia in data)
                {
                    MediaDataTransferObject media = new MediaDataTransferObject(
                                                                        currentMedia.OriginalName,
                                                                        PicturePathConverter
                                                                            .GetPictureFullPath(
                                                                                        currentMedia.PosterPath),
                                                                        currentMedia.Overview,
                                                                        currentMedia.ReleaseDate,
                                                                        currentMedia.Popularity);
                    resultCollection.Add(media);
                }

                return resultCollection;
            }
            catch (NullReferenceException)
            {
                System.Diagnostics.Debug.WriteLine("Could not parse objects from the server!");

                return new ObservableCollection<MediaDataTransferObject>();
            }
        }

        /// <summary>
        /// Convers list of <c>PersonInfo</c> into a collection of <c>PersonDataTransferObject</c>
        /// </summary>
        public static ObservableCollection<PersonDataTransferObject>
          ConvertPersonsToDataTransferCollection(List<PersonInfo> data)
        {
            try
            {
                ObservableCollection<PersonDataTransferObject> resultCollection =
                    new ObservableCollection<PersonDataTransferObject>();

                foreach (PersonInfo currentPerson in data)
                {
                    List<MediaDataTransferObject> filmography = new List<MediaDataTransferObject>();

                    foreach (MovieInfo currentMovie in currentPerson.KnownFor)
                    {
                        MediaDataTransferObject film = new MediaDataTransferObject(
                                                                            currentMovie.OriginalTitle,
                                                                            PicturePathConverter
                                                                                .GetPictureFullPath(
                                                                                    currentMovie.PosterPath),
                                                                            currentMovie.Overview,
                                                                            currentMovie.ReleaseDate,
                                                                            currentMovie.Popularity);

                        filmography.Add(film);
                    }

                    PersonDataTransferObject person = new PersonDataTransferObject(
                                                                            currentPerson.Name,
                                                                            PicturePathConverter
                                                                                .GetPictureFullPath(
                                                                                    currentPerson.PicturePath),
                                                                            currentPerson.Popularity,
                                                                            filmography);

                    resultCollection.Add(person);
                }

                return resultCollection;

            }
            catch (NullReferenceException)
            {
                System.Diagnostics.Debug.WriteLine("Could not parse objects from the server!");

                return new ObservableCollection<PersonDataTransferObject>();
            }
        }
    }

}

