﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Converters.PicturePath
{
    /// <summary>
    /// use this cass to conver urls froom relative picture paths
    /// </summary>
    public class PicturePathConverter
    {
        private const string baseUrl = @"http://image.tmdb.org/t/p/w500";

        /// <summary>
        /// Finds pictures abolute url by its relative path
        /// </summary>
        public static string GetPictureFullPath(string pictureID)
        {
            return baseUrl + pictureID;
        }
    }
}
