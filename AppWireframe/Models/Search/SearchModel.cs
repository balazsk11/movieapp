﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models
{
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Models.Base;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Model class for the <c>SearchResultViewModel</c>
    /// </summary>
    public class SearchModel : BrowserModelBase
    {
        /// <summary>
        /// Gets or sets the active search type
        /// </summary>
        public string ActiveSearchType { get; set; }

        /// <summary>
        /// Gets or sets he keyword of the search
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// Conatins the results of movie search
        /// </summary>
        public ObservableCollection<MediaDataTransferObject> Movies { get; set; }

        /// <summary>
        /// Conatins the results of people search
        /// </summary>
        public ObservableCollection<PersonDataTransferObject> People { get; set; }

        /// <summary>
        /// Conatins the results of TV Show search
        /// </summary>
        public ObservableCollection<MediaDataTransferObject> TvShows { get; set; }
    }
}
