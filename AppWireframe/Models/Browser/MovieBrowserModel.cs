﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models
{
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Models.Base;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Model class for the <c>MovieBrowserViewModel</c>
    /// Contains data to be displayed on the UI
    /// </summary>
    public class MovieBrowserModel : BrowserModelBase
    {
        /// <summary>
        /// Gets or sets the Movies that appear on the UI
        /// </summary>
        public ObservableCollection<MediaDataTransferObject> Movies { get; set; }
    }
}
