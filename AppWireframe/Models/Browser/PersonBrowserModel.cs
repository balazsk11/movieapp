﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models
{
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Models.Base;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Model class for the <c>PersonBrowserViewModel</c>
    /// Contains data to be displayed on the UI
    /// </summary>
    public class PersonBrowserModel : BrowserModelBase
    {
        /// <summary>
        /// Gets or sets the People that appear on the UI
        /// </summary>
        public ObservableCollection<PersonDataTransferObject> People { get; set; }
    }
}
