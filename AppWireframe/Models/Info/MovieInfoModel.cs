﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models
{
    using AppWireframe.DataTransferObjects;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Contains info about Movies to be presented on the UI
    /// </summary>
    public class MovieInfoModel : ObservableObject
    {
        /// <summary>
        /// Gets or set the properties of a Movie
        /// </summary>
        public MediaDataTransferObject Movie { get; set; }
    }
}
