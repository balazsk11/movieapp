﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models
{
    using AppWireframe.DataTransferObjects;
    using Base;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Contains info about People to be presented on the UI
    /// </summary>
    public class PersonInfoModel : BrowserModelBase
    {
        /// <summary>
        /// Gets or set the properties of a Person
        /// </summary>
        public PersonDataTransferObject Person { get; set; }
    }
}
