﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models.Info
{
    using AppWireframe.DataTransferObjects;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Contains info about TVShows to be presented on the UI
    /// </summary>
    public class TvShowInfoModel : ObservableObject
    {
        /// <summary>
        /// Gets or set the properties of a TvShow
        /// </summary>
        public MediaDataTransferObject TvShow { get; set; }
    }
}
