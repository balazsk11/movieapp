﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Models.Base
{
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Base calls for models where items can be selected
    /// </summary>
    public class BrowserModelBase : ObservableObject
    {
        /// <summary>
        /// Gets or sets the SelectedIndex of a collection
        /// </summary>
        public int? SelectedIndex { get; set; }
    }
}
