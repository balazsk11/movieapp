﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.SystemSettings
{
    /// <summary>
    /// View name keys are registered here
    /// </summary>
    public static class ViewNames
    {
        /// <summary>
        /// View key to use when navigation to the <c>MainPage</c>
        /// </summary>
        public const string MainPageName = "MainPage";

        /// <summary>
        /// View key to use when navigation to the <c>MovieInfoPage</c>
        /// </summary>
        public const string MovieInfoPageName = "MovieInfoPage";

        /// <summary>
        /// View key to use when navigation to the <c>TvShowInfoPage</c>
        /// </summary>
        public const string TvShowInfoPageName = "TvShowInfoPage";

        /// <summary>
        /// View key to use when navigation to the <c>PersonInfoPage</c>
        /// </summary>
        public const string PersonInfoPageName = "PersonInfoPage";

        /// <summary>
        /// View key to use when navigation to the <c>SearchResultPage</c>
        /// </summary>
        public const string SearchResultPageName = "SearchResultPage";
    }
}