﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.SystemSettings
{
    /// <summary>
    /// Contains necessary constatn values for the system
    /// </summary>
    public static class SystemConstants
    {
        /// <summary>
        /// Movie type of search
        /// </summary>
        public const string SearchType_Movie = "movie";

        /// <summary>
        /// Tv Show type of search
        /// </summary>
        public const string SearchType_TvShow = "tvshow";

        /// <summary>
        /// People type of search
        /// </summary>
        public const string SearchType_People = "people";

        /// <summary>
        /// Identifies the search error text in the string resources
        /// </summary>
        public const string SearchErrorId = "SearchError";

        /// <summary>
        /// Identifies server response errer text in the string resources
        /// </summary>
        public const string ServerResponseError = "ServerResponseError";
    }
}