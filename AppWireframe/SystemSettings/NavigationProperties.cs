﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.SystemSettings
{
    using AppWireframe.Views;
    using GalaSoft.MvvmLight.Views;

    /// <summary>
    /// Navigation service properties
    /// </summary>
    public static class NavigationProperties
    {
        /// <summary>
        /// Gets the default instance of Navigation Service
        /// Use this for navigation
        /// </summary>
        public static NavigationService DefaultNavigationService { get; private set; }

        /// <summary>
        /// Register pages for their navigation key
        /// </summary>
        public static void InitNavigation()
        {
            DefaultNavigationService = new NavigationService();
            DefaultNavigationService.Configure(ViewNames.MainPageName, typeof(MainPage));
            DefaultNavigationService.Configure(ViewNames.MovieInfoPageName, typeof(MovieInfoPage));
            DefaultNavigationService.Configure(ViewNames.TvShowInfoPageName, typeof(TvShowInfoPage));
            DefaultNavigationService.Configure(ViewNames.PersonInfoPageName, typeof(PersonInfoPage));
            DefaultNavigationService.Configure(ViewNames.SearchResultPageName, typeof(SearchResultPage));
        }
    }
}
