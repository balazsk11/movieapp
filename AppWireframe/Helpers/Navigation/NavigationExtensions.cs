﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Helpers.Navigation
{
    using AppWireframe.SystemSettings;
    using GalaSoft.MvvmLight.Views;
    using System.Collections.Generic;

    /// <summary>
    /// This class contains extension methods for navigation
    /// </summary>
    public static class NavigationExtensions
    {
        private static Stack<KeyValuePair<string, object>> navigationStack;

        /// <summary>
        /// Navigates to the given page
        /// </summary>
        /// <param name="navigationService">the navigation service class that is extended</param>
        /// <param name="pageKey">
        /// the key of the destination page
        /// Use <c>ViewNames</c> and <c>NavigationProperties</c> to register and read available page keys
        /// </param>
        /// <param name="parameter">the parameter to be passed for the next page <c>null</c> by default</param>
        public static void Navigate(this INavigationService navigationService, string pageKey, object parameter = null)
        {
            navigationStack.Push(new KeyValuePair<string, object>(pageKey, parameter));

            navigationService.NavigateTo(pageKey, parameter);
        }

        /// <summary>
        /// Navigates back to the previous page
        /// </summary>
        /// <param name="navigationService">the navigation service class that is extended</param>
        public static void NavigateBack(this INavigationService navigationService)
        {
            if (navigationStack.Count > 1)
            {
                navigationStack.Pop();
            }
            
            KeyValuePair<string, object> previousPage = navigationStack.Peek();

            navigationService.NavigateTo(previousPage.Key, previousPage.Value);
        }

        /// <summary>
        /// initializes the navigation stack
        /// </summary>
        public static void Init()
        {
            navigationStack = new Stack<KeyValuePair<string, object>>();
            navigationStack.Push(new KeyValuePair<string, object>(ViewNames.MainPageName, null));
        }
    }
}
