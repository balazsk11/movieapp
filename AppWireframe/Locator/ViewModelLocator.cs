﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Locator
{
    using AppWireframe.Services.Movies;
    using AppWireframe.Services.Persons;
    using AppWireframe.Services.Search.Movies;
    using AppWireframe.Services.Search.Persons;
    using AppWireframe.Services.Search.TvShows;
    using AppWireframe.Services.TvShows;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels;
    using AppWireframe.ViewModels.Infos;
    using GalaSoft.MvvmLight.Ioc;
    using Microsoft.Practices.ServiceLocation;

    /// <summary>
    // This class creates ViewModels for the views
    /// </summary>
    public class ViewModelLocator
    {

        /// <summary>
        /// Creates an instance of view model locator and inits the navigation
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            NavigationProperties.InitNavigation();
        }

        /// <summary>
        /// Gets viewmodel that handles movie informations and functions
        /// </summary>
        public MovieInfoViewModel MovieInfo
        {
            get
            {
                return new MovieInfoViewModel(NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles Tv Show informations and functions
        /// </summary>
        public TvShowInfoViewModel TvShowInfo
        {
            get
            {
                return new TvShowInfoViewModel(NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles movie informations and functions
        /// </summary>
        public PersonInfoViewModel PersonInfo
        {
            get
            {
                return new PersonInfoViewModel(NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles movie discovery informations and functions
        /// </summary>
        public MovieBrowserViewModel Movies
        {
            get
            {
                return new MovieBrowserViewModel(new MoviesService(), NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles TV Show discovery informations and functions
        /// </summary>
        public TvShowBrowserViewModel TvShows
        {
            get
            {
                return new TvShowBrowserViewModel(new TvShowsService(), NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles person discovery informations and functions
        /// </summary>
        public PersonBrowserViewModel Persons
        {
            get
            {
                return new PersonBrowserViewModel(new PersonsService(), NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles movie, person and TV Show search informations and functions
        /// </summary>
        public SearchViewModel Search
        {
            get
            {
                return new SearchViewModel(
                    new MovieSearchService(),
                    new PersonSearchService(),
                    new TvShowSearchService(),
                    NavigationProperties.DefaultNavigationService);
            }
        }

        /// <summary>
        /// Gets viewmodel that handles movie, person and TV Show search result informations and functions
        /// </summary>
        public SearchResultViewModel SearchResult
        {
            get
            {
                return new SearchResultViewModel(NavigationProperties.DefaultNavigationService);
            }
        }
    }
}