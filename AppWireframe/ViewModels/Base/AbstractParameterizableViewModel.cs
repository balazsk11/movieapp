﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels.Base
{
    using AppWireframe.ViewModels.Base.Launch;
    using AppWireframe.ViewModels.Launch;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Views;

    /// <summary>
    ///  Abstract class fo parameterizable view models
    /// </summary>
    /// <typeparam name="T">
    /// A model for the View-Model
    /// must be inherited from <c>ObservableObject</c>
    /// </typeparam>
    public abstract class AbstractParameterizableViewModel<T> 
        : AbstractViewModel<T>, IParameterizableViewModel where T: ObservableObject
    {
        /// <summary>
        /// event handler for launched events
        /// </summary>
        public event LaunchedEventHandler Launched;

        /// <summary>
        /// abstract method
        /// calls this when <c>Launched</c> event occurs
        /// </summary>
        protected abstract void OnLaunched(object sender, LaunchedEventArgs launchedEventArgs);

        /// <summary>
        /// <c>NavigationService</c> instance that handles the navigation
        /// </summary>
        protected INavigationService NavigationService { get; set; }

        /// <summary>
        /// Use this event from an outer class to fire <c>Launched</c> event
        /// </summary>
        public void RaiseLaunched(object sender, LaunchedEventArgs launchedEventArgs)
        {
            if (Launched != null)
            {
                Launched(sender, launchedEventArgs);
            }
        }
    }
}