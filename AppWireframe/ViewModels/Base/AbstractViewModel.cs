﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels.Base
{
    using GalaSoft.MvvmLight;
    using System;

    /// <summary>
    ///  Abstract class fo view models
    /// </summary>
    /// <typeparam name="T">
    /// A model for the View-Model
    /// must be inherited from <c>ObservableObject</c>
    /// </typeparam>
    public abstract class AbstractViewModel<T> : ViewModelBase  where T: ObservableObject
    {
        private T model;

        /// <summary>
        /// Model for the view
        /// </summary>
        public T Model
        {
            get
            {
                if (model == null)
                {
                    model = new Lazy<T>().Value;
                    return model;
                }
                else
                {
                    return model;
                }
            }

            set
            {
                model = value;
            }
        }
    }
}