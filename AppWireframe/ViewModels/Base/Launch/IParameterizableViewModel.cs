﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels.Launch
{
    using AppWireframe.ViewModels.Base.Launch;

    /// <summary>
    /// This interface abstracts functions fro parameterizble navigation
    /// </summary>
    public interface IParameterizableViewModel
    {
        /// <summary>
        /// Raises the launched event
        /// </summary>
        void RaiseLaunched(object sender, LaunchedEventArgs launchedEventArgs);

        /// <summary>
        /// Indicates if the viemodel was launched
        /// </summary>
        event LaunchedEventHandler Launched;
    }
}
