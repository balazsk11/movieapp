﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels.Base.Launch
{
    using System;

    /// <summary>
    /// event handler for the <c>Launched</c> event
    /// </summary>
    public delegate void LaunchedEventHandler(object sender, LaunchedEventArgs e);

    /// <summary>
    /// event args for the <c>Launched</c> event
    /// </summary>
    public class LaunchedEventArgs : EventArgs
    {
        /// <summary>
        /// The parameter got from the calling page
        /// </summary>
        public object NavigationParameter { get; private set; }

        /// <summary>
        /// Creates an instence of <c>LaunchedEventArgs</c>
        /// </summary>
        /// <param name="parameter">The parameter got from the calling page</param>
        public LaunchedEventArgs(object parameter)
        {
            NavigationParameter = parameter;
        }
    }
}
