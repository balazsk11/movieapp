﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels
{
    using AppWireframe.Beans;
    using AppWireframe.Converters.MediaResponseConverter;
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models;
    using AppWireframe.Services.Search.Movies;
    using AppWireframe.Services.Search.Persons;
    using AppWireframe.Services.Search.TvShows;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels.Base;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Windows.ApplicationModel.Resources;
    using Windows.UI.Popups;

    /// <summary>
    /// view model for search functions
    /// </summary>
    public class SearchViewModel : AbstractViewModel<SearchModel>
    {
        private IMovieSearchService _movieSearchService;

        private IPersonSearchService _personSearchService;

        private ITvShowSearchService _tvShowSearchService;

        private INavigationService _navigationService;

        private ResourceLoader _resourceLoader;

        /// <summary>
        /// This command fires when the search occures
        /// </summary>
        public ICommand SearchCommand { get; set; }

        /// <summary>
        /// This command fires when the search context changes 
        /// (together with the pivot)
        /// </summary>
        public ICommand PivotSelectionChangedCommand { get; set; }

        /// <summary>
        /// Creates an instance of <c>SearchViewModel</c>
        /// </summary>
        public SearchViewModel(
                        IMovieSearchService movieSearchService,
                        IPersonSearchService personSearchService,
                        ITvShowSearchService tvShowSearchService,
                        INavigationService navigationService)
        {
            _movieSearchService = movieSearchService;
            _personSearchService = personSearchService;
            _tvShowSearchService = tvShowSearchService;
            _navigationService = navigationService;

            _resourceLoader = new ResourceLoader();

            SearchCommand = new RelayCommand(Search);

            PivotSelectionChangedCommand = new RelayCommand<object>(SelectionChanged);
        }

        private void SelectionChanged(object parameter)
        {
            if (parameter is int)
            {
                int index = int.Parse(parameter.ToString());
                switch (index)
                {
                    case 0:
                        Model.ActiveSearchType = SystemConstants.SearchType_Movie;
                        break;
                    case 1:
                        Model.ActiveSearchType = SystemConstants.SearchType_TvShow;
                        break;
                    case 2:
                        Model.ActiveSearchType = SystemConstants.SearchType_People;
                        break;
                    default:
                        throw new ArgumentException("Invalid Search Type");
                }
            }
            else
            {
                //becuse of technical reasons the initial value of "0" could not be caught
                //this is a workaround to create default behaviour

                Model.ActiveSearchType = SystemConstants.SearchType_Movie;
            }
        }

        private async void Search()
        {
            try
            {
                if (string.IsNullOrEmpty(Model.KeyWord))
                {
                    MessageDialog dialog = new MessageDialog(
                        _resourceLoader.GetString(SystemConstants.SearchErrorId));

                    await dialog.ShowAsync();

                    return;
                }

                switch (Model.ActiveSearchType)
                {
                    case SystemConstants.SearchType_Movie:
                        await CreateMovieSearch();
                        break;
                    case SystemConstants.SearchType_TvShow:
                        await CreateTvShowSearch();
                        break;
                    case SystemConstants.SearchType_People:
                        await CreatePeopleSearch();
                        break;
                    default:
                        throw new ArgumentException("Invalid Search Type");
                }

                _navigationService.Navigate(ViewNames.SearchResultPageName, Model);
            }
            catch (AggregateException exception)
            {
                MessageDialog dialog = new MessageDialog(
                    _resourceLoader.GetString(SystemConstants.ServerResponseError));

                await dialog.ShowAsync();

                System.Diagnostics.Debug.WriteLine(exception.Flatten().Data);
            }
        }

        private async Task CreateMovieSearch()
        {
            MovieResponse searchResponse = await _movieSearchService.SearchMoviesAsync(Model.KeyWord);
            List<MovieInfo> data = searchResponse.Results;

            Model.Movies = ResponeToObservableMediaDataTransferObjectConverter
                .ConvertMoviesToDataTransferCollection(data);
        }

        private async Task CreateTvShowSearch()
        {
            TvShowsResponse searchResponse = await _tvShowSearchService.SearchTvShowsAsync(Model.KeyWord);
            List<TvShowInfo> data = searchResponse.Results;

            Model.TvShows = ResponeToObservableMediaDataTransferObjectConverter
                .ConvertTvShowsToDataTransferCollection(data);
        }

        private async Task CreatePeopleSearch()
        {
            PersonResponse searchResponse = await _personSearchService.SearchPersonsAsync(Model.KeyWord);
            List<PersonInfo> data = searchResponse.Results;

            Model.People = ResponeToObservableMediaDataTransferObjectConverter
                .ConvertPersonsToDataTransferCollection(data);
        }
    }
}
