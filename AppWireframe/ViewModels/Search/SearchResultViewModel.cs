﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels
{
    using AppWireframe.DataTransferObjects;
    using AppWireframe.DataTransferObjects.Base;
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels.Base;
    using AppWireframe.ViewModels.Base.Launch;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;

    /// <summary>
    /// this view model contains the results of a search
    /// </summary>
    public class SearchResultViewModel : AbstractParameterizableViewModel<SearchModel>
    {
        public ObservableCollection<DataTransferObjectBase> Items { get; set; }

        public ICommand SelectCommand { get; set; }

        /// <summary>
        /// Gets or sets a command that performs bacward navigation
        /// </summary>
        public ICommand NavigateBackCommand { get; set; }


        /// <summary>
        /// Creates an instance of <c>SearchResultViewModel</c>
        /// </summary>
        public SearchResultViewModel(INavigationService navigationservice)
        {
            Launched += OnLaunched;

            NavigationService = navigationservice;

            SelectCommand = new RelayCommand(SelectItem);

            NavigateBackCommand = new RelayCommand(NavigateBack);
        }

        private void NavigateBack()
        {
            NavigationService.NavigateBack();
        }

        private void SelectItem()
        {
            switch (Model.ActiveSearchType)
            {
                case SystemConstants.SearchType_Movie:
                    NavigationService.Navigate(
                        ViewNames.MovieInfoPageName,
                        Model.Movies.ElementAt(Model.SelectedIndex.Value));
                    break;

                case SystemConstants.SearchType_TvShow:
                    NavigationService.Navigate(
                        ViewNames.TvShowInfoPageName,
                        Model.TvShows.ElementAt(Model.SelectedIndex.Value));
                    break;

                case SystemConstants.SearchType_People:
                    NavigationService.Navigate(
                        ViewNames.PersonInfoPageName,
                        Model.People.ElementAt(Model.SelectedIndex.Value));
                    break;

                default:
                    throw new ArgumentException("Invalid Search type");
            }
        }

        /// <summary>
        /// Called when the connected biewe is launched
        /// </summary>
        protected override void OnLaunched(object sender, LaunchedEventArgs launchedEventArgs)
        {
            Model = launchedEventArgs.NavigationParameter as SearchModel;

            switch (Model.ActiveSearchType)
            {
                case SystemConstants.SearchType_Movie:
                    Items = CreateMediaSearchReults(Model.Movies);
                    break;

                case SystemConstants.SearchType_People:
                    Items = CreatePersonSearchReults(Model.People);
                    break;

                case SystemConstants.SearchType_TvShow:
                    Items = CreateMediaSearchReults(Model.TvShows);
                    break;

                default:
                    throw new ArgumentException("Invalid Search type");
            }
        }

        private ObservableCollection<DataTransferObjectBase> CreatePersonSearchReults(
            ObservableCollection<PersonDataTransferObject> people)
        {
            ObservableCollection<DataTransferObjectBase> resultCollection
                = new ObservableCollection<DataTransferObjectBase>();

            foreach (PersonDataTransferObject currentPerson in people)
            {
                DataTransferObjectBase extract = currentPerson;

                resultCollection.Add(extract);
            }

            return resultCollection;
        }

        private ObservableCollection<DataTransferObjectBase> CreateMediaSearchReults(
            ObservableCollection<MediaDataTransferObject> medias)
        {
            ObservableCollection<DataTransferObjectBase> resultCollection
                = new ObservableCollection<DataTransferObjectBase>();

            foreach (MediaDataTransferObject currentMedia in medias)
            {
                DataTransferObjectBase extract = currentMedia; 

                resultCollection.Add(extract);
            }

            return resultCollection;
        }
    }
}
