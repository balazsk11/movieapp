﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

using AppWireframe.DataTransferObjects;
using AppWireframe.Helpers.Navigation;
using AppWireframe.Models;
using AppWireframe.ViewModels.Base;
using AppWireframe.ViewModels.Base.Launch;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using System.Windows.Input;
using System;
using AppWireframe.SystemSettings;
using System.Linq;

namespace AppWireframe.ViewModels
{
    /// <summary>
    /// Contains info about a person
    /// Use together with an adequate view by bounding its commands and data
    /// </summary>
    public class PersonInfoViewModel : AbstractParameterizableViewModel<PersonInfoModel>
    {
        /// <summary>
        /// Gets or sets a command that performs bacward navigation
        /// </summary>
        public ICommand NavigateBackCommand { get; set; }

        /// <summary>
        /// A command that fires when amovie is selected
        /// </summary>
        public ICommand SelectMovieCommand { get; set; }

        /// <summary>
        /// Creates an instance of <c>PersonInfoViewModel</c>
        /// Use to setup functionality of a view
        /// </summary>
        /// <param name="naigationService">An instance of <c>INavigationService</c> to perform navigation</param>
        public PersonInfoViewModel(INavigationService navigationService)
        {
            Launched += OnLaunched;
            NavigationService = navigationService;

            NavigateBackCommand = new RelayCommand(NavigateBack);
            SelectMovieCommand = new RelayCommand(SelectMovie);
        }

        private void SelectMovie()
        {
            NavigationService.Navigate(ViewNames.MovieInfoPageName, Model.Person.Filmography.ElementAt(Model.SelectedIndex.Value));
        }

        /// <summary>
        /// Use for initiating the viewModel after a parameterized navigation
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="launchedEventArgs">Here you can find the parameter for the navigation</param>
        protected override void OnLaunched(object sender, LaunchedEventArgs launchedEventArgs)
        {
            PersonDataTransferObject person = launchedEventArgs.NavigationParameter as PersonDataTransferObject;

            Model.Person = person;
        }

        private void NavigateBack()
        {
            NavigationService.NavigateBack();
        }
    }
}