﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels.Infos
{
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models.Info;
    using AppWireframe.ViewModels.Base;
    using AppWireframe.ViewModels.Base.Launch;
    using DataTransferObjects;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Contains info about a TV Show
    /// Use together with an adequate view by bounding its commands and data
    /// </summary>
    public class TvShowInfoViewModel : AbstractParameterizableViewModel<TvShowInfoModel>
    {
        /// <summary>
        /// Gets or sets a command that performs bacward navigation
        /// </summary>
        public ICommand NavigateBackCommand { get; set; }

        /// <summary>
        /// Creates an instance of <c>TvShowInfoViewModel</c>
        /// Use to setup functionality of a view
        /// </summary>
        /// <param name="navigationService">An instance of <c>INavigationService</c> to perform navigation</param>
        public TvShowInfoViewModel(INavigationService navigationService)
        {
            Launched += OnLaunched;
            NavigationService = navigationService;

            NavigateBackCommand = new RelayCommand(NavigateBack);
        }

        /// <summary>
        /// Use for initiating the viewModel after a parameterized navigation
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        /// <param name="launchedEventArgs">Here you can find the parameter for the navigation</param>
        protected override void OnLaunched(object sender, LaunchedEventArgs launchedEventArgs)
        {
            MediaDataTransferObject tvShow = launchedEventArgs.NavigationParameter as MediaDataTransferObject;

            Model.TvShow = tvShow;
        }

        private void NavigateBack()
        {
            NavigationService.NavigateBack();
        }
    }
}
