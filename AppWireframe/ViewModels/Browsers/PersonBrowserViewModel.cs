﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels
{
    using AppWireframe.Beans;
    using AppWireframe.Converters.MediaResponseConverter;
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models;
    using AppWireframe.Services.Persons;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels.Base;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Windows.ApplicationModel.Resources;
    using Windows.UI.Popups;

    /// <summary>
    /// View modell for people browsing
    /// </summary>
    public class PersonBrowserViewModel : AbstractViewModel<PersonBrowserModel>
    {
        private IPersonsService _personsService;

        private INavigationService _navigationService;

        private ResourceLoader _resourceLoader;

        /// <summary>
        /// Commands that fires when a person is selected
        /// </summary>
        public ICommand SelectPersonCommand { get; set; }

        /// <summary>
        /// creates an instance of  <c>PersonBrowserViewModel</c>
        /// </summary>
        public PersonBrowserViewModel(IPersonsService personsService, INavigationService navigationService)
        {
            _personsService = personsService;
            _navigationService = navigationService;

            _resourceLoader = new ResourceLoader();

            Model.People = GetPeopleInforamtionAsync().Result;

            SelectPersonCommand = new RelayCommand(SelectPerson);
        }

        private void SelectPerson()
        {
            _navigationService.Navigate(ViewNames.PersonInfoPageName, Model.People.ElementAt(Model.SelectedIndex.Value));
        }

        private async Task<ObservableCollection<PersonDataTransferObject>> GetPeopleInforamtionAsync()
        {
            try
            {
                PersonResponse response = await _personsService.GetPersonsAsync();
                List<PersonInfo> data = response.Results;

                ObservableCollection<PersonDataTransferObject> resultCollection =
                    ResponeToObservableMediaDataTransferObjectConverter.ConvertPersonsToDataTransferCollection(data);

                return Task.Run<ObservableCollection<PersonDataTransferObject>>(
                    () =>
                    {
                        return resultCollection;
                    }).Result;
            }
            catch (AggregateException exception)
            {
                MessageDialog dialog = new MessageDialog(
                    _resourceLoader.GetString(SystemConstants.ServerResponseError));

                await dialog.ShowAsync();

                System.Diagnostics.Debug.WriteLine(exception.Flatten().Data);

                return new ObservableCollection<PersonDataTransferObject>();
            }
        }
    }
}
