﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels
{
    using AppWireframe.Beans;
    using AppWireframe.Converters.MediaResponseConverter;
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models;
    using AppWireframe.Services.TvShows;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels.Base;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Windows.ApplicationModel.Resources;
    using Windows.UI.Popups;

    /// <summary>
    /// View modell for people browsing
    /// </summary>
    public class TvShowBrowserViewModel  : AbstractViewModel<TvShowBrowserModel>
    {
        private ITvShowsService _tvShowsService;

        private INavigationService _navigationService;

        private ResourceLoader _resourceLoader;

        /// <summary>
        /// Commands that fires when a tv show browser is selected
        /// </summary>
        public ICommand SelectTvShowCommand { get; set; }

        /// <summary>
        /// Creates an instance of <c>TvShowBrowserViewModel</c>
        /// </summary>
        public TvShowBrowserViewModel(ITvShowsService tvShowsService, INavigationService navigationService)
        {
            _tvShowsService = tvShowsService;
            _navigationService = navigationService;

            _resourceLoader = new ResourceLoader();

            Model.TvShows = GetMediaInforamtionAsync().Result;

            SelectTvShowCommand = new RelayCommand(SelectTvShow);

        }

        private void SelectTvShow()
        {
            _navigationService.Navigate(
                ViewNames.TvShowInfoPageName,
                Model.TvShows.ElementAt(Model.SelectedIndex.Value));
        }

        private async Task<ObservableCollection<MediaDataTransferObject>> GetMediaInforamtionAsync()
        {
            try
            {
                TvShowsResponse response = await _tvShowsService.GetTvShowsAsync();
                List<TvShowInfo> data = response.Results;

                ObservableCollection<MediaDataTransferObject> resultCollection =
                    ResponeToObservableMediaDataTransferObjectConverter.ConvertTvShowsToDataTransferCollection(data);

                return Task.Run<ObservableCollection<MediaDataTransferObject>>(
                    () =>
                    {
                        return resultCollection;
                    }).Result;
            }
            catch (AggregateException exception)
            {
                MessageDialog dialog = new MessageDialog(
                    _resourceLoader.GetString(SystemConstants.ServerResponseError));

                await dialog.ShowAsync();

                System.Diagnostics.Debug.WriteLine(exception.Flatten().Data);

                return new ObservableCollection<MediaDataTransferObject>();
            }
        }
    }
}
