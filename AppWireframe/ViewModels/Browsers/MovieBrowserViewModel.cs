﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.ViewModels
{
    using AppWireframe.Beans;
    using AppWireframe.Converters.MediaResponseConverter;
    using AppWireframe.DataTransferObjects;
    using AppWireframe.Helpers.Navigation;
    using AppWireframe.Models;
    using AppWireframe.Services.Movies;
    using AppWireframe.SystemSettings;
    using AppWireframe.ViewModels.Base;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Views;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Windows.ApplicationModel.Resources;
    using Windows.UI.Popups;

    /// <summary>
    /// View modell for movie browsing
    /// </summary>
    public class MovieBrowserViewModel : AbstractViewModel<MovieBrowserModel>
    {
        private IMoviesService _moviesService;

        private INavigationService _navigationService;

        private ResourceLoader _resourceLoader;

        /// <summary>
        /// A command thatfires when a movie is selected
        /// </summary>
        public ICommand SelectMovieCommand { get; set; }

        /// <summary>
        /// Creates an instance of <c>MovieBrowserViewModel</c>
        /// </summary>
        public MovieBrowserViewModel(IMoviesService mediasService, INavigationService navigationService)
        {
            _moviesService = mediasService;
            _navigationService = navigationService;

            _resourceLoader = new ResourceLoader();

            Model.Movies = GetMovieInforamtionAsync().Result;
            SelectMovieCommand = new RelayCommand(SelectMovie);
        }

        private void SelectMovie()
        {
            _navigationService.Navigate(
                ViewNames.MovieInfoPageName,
                Model.Movies.ElementAt(Model.SelectedIndex.Value));
        }

        private async Task<ObservableCollection<MediaDataTransferObject>> GetMovieInforamtionAsync()
        {
            try
            {
                MovieResponse response = await _moviesService.GetMoviesAsync();
                List<MovieInfo> data = response.Results;

                ObservableCollection<MediaDataTransferObject> resultCollection =
                    ResponeToObservableMediaDataTransferObjectConverter.ConvertMoviesToDataTransferCollection(data);

                return Task.Run<ObservableCollection<MediaDataTransferObject>>(
                    () =>
                    {
                        return resultCollection;
                    }).Result;

            }
            catch (AggregateException exception)
            {
                MessageDialog dialog = new MessageDialog(
                    _resourceLoader.GetString(SystemConstants.ServerResponseError));

                await dialog.ShowAsync();

                System.Diagnostics.Debug.WriteLine(exception.Flatten().Data);

                return new ObservableCollection<MediaDataTransferObject>();
            }
        }

    }
}
