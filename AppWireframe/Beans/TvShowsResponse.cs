﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains the TV show responses from the server
    /// </summary>
    [DataContract]
    public class TvShowsResponse : ResponseBase
    {
        /// <summary>
        /// The list of information about TV Shows in the current page
        /// </summary>
        [DataMember(Name = "results")]
        public List<TvShowInfo> Results { get; set; }
    }
}
