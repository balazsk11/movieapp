﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains the movie responses from the server
    /// </summary>
    [DataContract]
    public class MovieResponse : ResponseBase
    {
        /// <summary>
        /// The list of information about Movies in the current page
        /// </summary>
        [DataMember(Name = "results")]
        public List<MovieInfo> Results { get; set; }

    }
}
