﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains person related infos
    /// </summary>
    [DataContract]
    public class PersonInfo
    {
        /// <summary>
        /// relative path of a person's picture
        /// </summary>
        [DataMember(Name = "profile_path")]
        public string PicturePath { get; set; }

        /// <summary>
        /// indicates adult content
        /// </summary>
        [DataMember(Name = "adult")]
        public bool Adult { get; set; }

        /// <summary>
        /// the server id of the object
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// list of most succesfull movies
        /// </summary>
        [DataMember(Name = "known_for")]
        public List<MovieInfo> KnownFor { get; set; }

        /// <summary>
        /// the name of actor or actress
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        ///  the popularity of actor or actress
        /// </summary>
        [DataMember(Name = "popularity")]
        public double Popularity { get; set; }
    }
}
