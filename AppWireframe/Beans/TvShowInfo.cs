﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains information about a tv show
    /// </summary>
    [DataContract]
    public class TvShowInfo : MediaInfo
    {
        /// <summary>
        /// The Name of the tv show
        /// </summary>
        [DataMember(Name="original_name")]
        public string OriginalName { get; set; }
    }
}
