﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains the person responses from the server
    /// </summary>
    [DataContract]
    public class PersonResponse : ResponseBase
    {
        /// <summary>
        /// The list of information about Persons in the current page
        /// </summary>
        [DataMember(Name = "results")]
        public List<PersonInfo> Results { get; set; }
    }
}