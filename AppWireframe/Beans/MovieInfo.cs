﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains movie related info
    /// </summary>
    [DataContract]
    public class MovieInfo : MediaInfo
    {
        /// <summary>
        /// the original title of a movie
        /// </summary>
        [DataMember(Name = "original_title")]
        public string OriginalTitle { get; set; }
    }
}
