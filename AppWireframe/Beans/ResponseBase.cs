﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Base clas for response from the server
    /// </summary>
    [DataContract]
    public class ResponseBase
    {
        /// <summary>
        /// the actual page
        /// </summary>
        [DataMember(Name = "page")]
        public int Page { get; set; }

        /// <summary>
        /// the total number of results
        /// </summary>
        [DataMember(Name = "total_results")]
        public int TotalResults { get; set; }

        /// <summary>
        /// the total pages of results
        /// </summary>
        [DataMember(Name = "total_pages")]
        public int TotalPages { get; set; }

    }
}
