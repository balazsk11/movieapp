﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Beans
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contains media related info (movie or tv show currently)
    /// </summary>
    [DataContract]
    public class MediaInfo
    {
        /// <summary>
        /// the realtive path of the poster
        /// </summary>
        [DataMember(Name = "poster_path")]
        public string PosterPath { get; set; }

        /// <summary>
        /// indicates adult contnet
        /// </summary>
        [DataMember(Name = "adult")]
        public bool Adult { get; set; }

        /// <summary>
        /// short description of a media
        /// </summary>
        [DataMember(Name = "overview")]
        public string Overview { get; set; }

        /// <summary>
        /// Date of release
        /// </summary>
        [DataMember(Name = "release_date", IsRequired = false)]
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// an id form the server
        /// </summary>
        [DataMember(Name = "genre_ids")]
        public List<int> GenreIds { get; set; }

        /// <summary>
        /// another id from the server
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// the original language of the media
        /// </summary>
        [DataMember(Name = "original_language")]
        public string OriginalLanguage { get; set; }

        /// <summary>
        /// the title of the media
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }

        /// <summary>
        /// another picture ot he media
        /// </summary>
        [DataMember(Name = "backdrop_path")]
        public string BackdropPath { get; set; }

        /// <summary>
        /// inicates the popularity of a media
        /// </summary>
        [DataMember(Name = "popularity")]
        public double Popularity { get; set; }

        /// <summary>
        /// The number of votes
        /// </summary>
        [DataMember(Name = "vote_count")]
        public double VoteCount { get; set; }

        /// <summary>
        /// Show if a media has video or not
        /// </summary>
        [DataMember(Name = "video")]
        public bool Video { get; set; }

        /// <summary>
        /// The average number of votes
        /// </summary>
        [DataMember(Name = "vote_average")]
        public bool VoteAverage { get; set; }
    }
}
