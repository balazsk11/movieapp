﻿using AppWireframe.ViewModels.Base.Launch;
using AppWireframe.ViewModels.Launch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace AppWireframe.Views.Parameterizable
{
    /// <summary>
    /// Base class to support navigation and paramter passing before pages
    /// </summary>
    public class ParameterizablePage : Page
    {
        protected IParameterizableViewModel viewModel;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (viewModel != null)
            {
                viewModel.RaiseLaunched(this, new LaunchedEventArgs(e.Parameter));
            }

            base.OnNavigatedTo(e);
        }
    }
}
