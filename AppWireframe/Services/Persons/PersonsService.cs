﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Persons
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// This class implement function that discovers people
    /// </summary>
    public class PersonsService : AbstractGatewayService, IPersonsService
    {
        /// <summary>
        /// The location of people discovery service
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/person/popular";
            }
        }

        /// <summary>
        /// Gets the discovery of persons
        /// </summary>
        public async Task<PersonResponse> GetPersonsAsync()
        {
            Task<PersonResponse> task = Task.Run<PersonResponse>(
                async () =>
                {
                     string personURL = baseUrl + ServiceLocation + ApiKeyParameter;
                     HttpResponseMessage personResponse = await client.GetAsync(personURL);
                     string personResult = await personResponse.Content.ReadAsStringAsync();
                     return JsonConvert.DeserializeObject<PersonResponse>(personResult, settings);
                });

            task.Wait();
            return await task;
        }
    }
}
