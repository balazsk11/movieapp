﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Persons
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    /// This interface abstracts functions for people discovery
    /// </summary>
    public interface IPersonsService
    {
        /// <summary>
        /// Gets the discovery of persons
        /// </summary>
        Task<PersonResponse> GetPersonsAsync();
    }
}
