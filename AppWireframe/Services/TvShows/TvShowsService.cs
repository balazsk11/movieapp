﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.TvShows
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// This class implement function that discovers tv shows
    /// </summary>
    public class TvShowsService : AbstractGatewayService, ITvShowsService
    {
        /// <summary>
        /// The location of tvshow discovery
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/discover/tv";
            }
        }

        /// <summary>
        /// Gets tv shows
        /// </summary>
        public async Task<TvShowsResponse> GetTvShowsAsync()
        {
            Task<TvShowsResponse> task = Task.Run<TvShowsResponse>(
                 async () =>
                 {
                     string urlTV = baseUrl + ServiceLocation + ApiKeyParameter + @"&sort_by=popularity.desc";
                     var responseTV = await client.GetAsync(urlTV);
                     var resultTV = await responseTV.Content.ReadAsStringAsync();
                     return JsonConvert.DeserializeObject<TvShowsResponse>(resultTV, settings);
                 });
            task.Wait();
            return await task;
        }
    }
}