﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.TvShows
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    /// This interface abstracts  TV Show discovery
    /// </summary>
    public interface ITvShowsService
    {
        /// <summary>
        /// Discovers TvShows that are in interest
        /// </summary>
        Task<TvShowsResponse> GetTvShowsAsync();
    }
}
