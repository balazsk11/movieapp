﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.TvShows
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// This class implements functions that searches tv shows
    /// </summary>
    public class TvShowSearchService : AbstractGatewayService, ITvShowSearchService
    {
        /// <summary>
        /// the location of TvShow search service
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/search/tv";
            }
        }

        /// <summary>
        /// Searches TvShows
        /// </summary>
        /// <param name="keyWord">the keyword of the search</param>
        public async Task<TvShowsResponse> SearchTvShowsAsync(string keyWord)
        {
            Task<TvShowsResponse> task = Task.Run<TvShowsResponse>(
                async () => 
                {
                    string searchTvUrl = baseUrl + ServiceLocation + ApiKeyParameter + @"&query=" + keyWord;
                    var searchTvResponse = await client.GetAsync(searchTvUrl);
                    var searchTvResult = await searchTvResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TvShowsResponse>(searchTvResult, settings);
                });

            task.Wait();
            return await task;
        }
    }
}
