﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.TvShows
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    /// This interface abstract functions for searching movies
    /// </summary>
    public interface ITvShowSearchService
    {
        /// <summary>
        /// Searches tvshows
        /// </summary>
        /// <param name="keyWord">the keyword of the search</param>
        Task<TvShowsResponse> SearchTvShowsAsync(string keyWord);
    }
}
