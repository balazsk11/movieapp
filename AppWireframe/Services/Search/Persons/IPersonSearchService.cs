﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.Persons
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    /// This interface abstract functions for searching movies
    /// </summary>
    public interface IPersonSearchService
    {
        /// <summary>
        /// Searches people
        /// </summary>
        /// <param name="keyWord">the keyword of the search</param>
        Task<PersonResponse> SearchPersonsAsync(string keyWord);
    }
}
