﻿// -------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.Persons
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// This class implements functions that searches people
    /// </summary>
    public class PersonSearchService : AbstractGatewayService, IPersonSearchService
    {
        /// <summary>
        /// the location of person search service
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/search/person";
            }
        }

        /// <summary>
        /// Searches people
        /// </summary>
        /// <param name="keyWord">the keyword of the search</param>
        public async Task<PersonResponse> SearchPersonsAsync(string keyWord)
        {
            Task<PersonResponse> task = Task.Run<PersonResponse>(
              async () =>
              {
                  string searchPerson = baseUrl + ServiceLocation + ApiKeyParameter + @"&query=" + keyWord;
                  HttpResponseMessage searchPersonResponse = await client.GetAsync(searchPerson);
                  string searchPersonResult = await searchPersonResponse.Content.ReadAsStringAsync();

                  return JsonConvert.DeserializeObject<PersonResponse>(searchPersonResult, settings);
              });

            task.Wait();

            return await task;
        }
    }
}
