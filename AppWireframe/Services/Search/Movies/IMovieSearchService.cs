﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.Movies
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    /// This interface abstract functions for searching movies
    /// </summary>
    public interface IMovieSearchService
    {
        /// <summary>
        /// Searches movies
        /// </summary>
        /// <param name="keyWord">the keyword of the search</param>
        Task<MovieResponse> SearchMoviesAsync(string keyWord);
    }
}
