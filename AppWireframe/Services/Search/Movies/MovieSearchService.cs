﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// -------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Search.Movies
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// This class implements functions that searches movies
    /// </summary>
    public class MovieSearchService : AbstractGatewayService, IMovieSearchService
    {
        /// <summary>
        /// the location of movie search service
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/search/movie";
            }
        }

        /// <summary>
        /// the location of movies search service
        /// </summary>
        public async Task<MovieResponse> SearchMoviesAsync(string keyWord)
        {
            Task<MovieResponse> task = Task.Run<MovieResponse>(
               async () =>
               {
                   string searchMovieURL = baseUrl + ServiceLocation + ApiKeyParameter + @"&query=" + keyWord;
                   HttpResponseMessage searchMovieResponse = await client.GetAsync(searchMovieURL);
                   var searchMovieResult = await searchMovieResponse.Content.ReadAsStringAsync();
                   return JsonConvert.DeserializeObject<MovieResponse>(searchMovieResult, settings);
               });

            task.Wait();
            return await task;
        }
    }
}
