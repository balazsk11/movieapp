﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Movies
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Base;
    using Newtonsoft.Json;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// A service class that class movie discovery
    /// </summary>
    public class MoviesService : AbstractGatewayService, IMoviesService
    {
        /// <summary>
        /// the location of movie discovery service
        /// </summary>
        protected override string ServiceLocation
        {
            get
            {
                return @"/discover/movie";
            }
        }

        /// <summary>
        /// Gets the discovery of movies
        /// </summary>
        public async Task<MovieResponse> GetMoviesAsync()
        {
            Task<MovieResponse> task = Task.Run<MovieResponse>(
                async () =>
                {
                    string moviesUrl = baseUrl + ServiceLocation + ApiKeyParameter + @"&sort_by=popularity.desc";
                    HttpResponseMessage moviesResponse = await client.GetAsync(moviesUrl);
                    string result = await moviesResponse.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MovieResponse>(result, settings);
                });
            task.Wait();
            return await task;
        }
    }
}