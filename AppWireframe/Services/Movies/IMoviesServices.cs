﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Movies
{
    using AppWireframe.Beans;
    using System.Threading.Tasks;

    /// <summary>
    ///  A an interface that abstracts movie discovery
    /// </summary>
    public interface IMoviesService
    {
        /// <summary>
        /// Gets the discovery of movies
        /// </summary>
        Task<MovieResponse> GetMoviesAsync();
    }
}
