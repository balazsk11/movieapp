﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace AppWireframe.Services.Base
{
    using Newtonsoft.Json;
    using System.Net.Http;

    /// <summary>
    /// Abstract class for services that call MovieDatabase API
    /// </summary>
    public abstract class AbstractGatewayService
    {
        private const string _apiKeyPrefix = @"?api_key=";

        private const string _apiKey = "0a08e38b874d0aa2d426ffc04357069d";

        /// <summary>
        /// basic url for database API
        /// </summary>
        protected const string baseUrl = @"http://api.themoviedb.org/3";

        /// <summary>
        /// Holds the api key parameter of the request
        /// </summary>
        protected string ApiKeyParameter
        {
            get
            {
                return _apiKeyPrefix + _apiKey;
            }
        }

        /// <summary>
        /// Contains datetime formatting currently
        /// </summary>
        protected JsonSerializerSettings settings = new JsonSerializerSettings { DateFormatString = "yyyy-MM-dd" };

        /// <summary>
        /// The client for creating web requests
        /// </summary>
        protected HttpClient client = new HttpClient();

        /// <summary>
        /// Location of the service request
        /// </summary>
        protected abstract string ServiceLocation { get; }
    }
}
