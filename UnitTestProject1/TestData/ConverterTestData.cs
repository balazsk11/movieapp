﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.TestData
{
    using AppWireframe.Beans;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Contains hard coded data for Converter unit tests
    /// </summary>
    public static class ConverterTestData
    {
        private static readonly DateTime _creationDate = DateTime.Now;

        /// <summary>
        /// A collection of movies wraped in <c>MovieInfo</c>
        /// </summary>
        public static List<MovieInfo> MovieInfosTestData
        {
            get
            {
                return CreateMovieInfos();
            }
        }

        /// <summary>
        /// A collection of TV Shows wraped in <c>TvShowInfo</c>
        /// </summary>
        public static List<TvShowInfo> TvShowInfosTestData
        {
            get
            {
                return CreateTvShowInfos();
            }
        }

        /// <summary>
        /// A collection of Persons wraped in <c>PersonInfo</c>
        /// </summary>
        public static List<PersonInfo> PersonInfosTestData
        {
            get
            {
                return CreatePersonInfos();
            }
        }

        private static List<PersonInfo> CreatePersonInfos()
        {
            List<PersonInfo> persons = new List<PersonInfo>();

            persons.Add(
                new PersonInfo
                {
                    Name = "name1",
                    PicturePath = "/Path1",
                    Popularity = 1.0,
                    KnownFor = new List<MovieInfo>()
                });

            persons.Add(
                new PersonInfo
                {
                    Name = "name2",
                    PicturePath = "/Path2",
                    Popularity = 2.0,
                    KnownFor = new List<MovieInfo>()
                });

            persons.Add(
                new PersonInfo
                {
                    Name = "name3",
                    PicturePath = "/Path3",
                    Popularity = 3.0,
                    KnownFor = new List<MovieInfo>()
                });

            return persons;
        }

        private static List<TvShowInfo> CreateTvShowInfos()
        {
            List<TvShowInfo> tvShows = new List<TvShowInfo>();

            tvShows.Add(
                new TvShowInfo
                {
                    OriginalName = "name1",
                    PosterPath = "/PosterPath1",
                    Overview = "overview1",
                    ReleaseDate = _creationDate,
                    Popularity = 1.0
                });

            tvShows.Add(
                new TvShowInfo
                {
                    OriginalName = "name2",
                    PosterPath = "/PosterPath2",
                    Overview = "overview2",
                    ReleaseDate = _creationDate,
                    Popularity = 2.0
                });

            tvShows.Add(
                new TvShowInfo
                {
                    OriginalName = "name3",
                    PosterPath = "/PosterPath3",
                    Overview = "overview3",
                    ReleaseDate = _creationDate,
                    Popularity = 3.0
                });

            return tvShows;
        }

        private static List<MovieInfo> CreateMovieInfos()
        {
            List<MovieInfo> movies = new List<MovieInfo>();

            movies.Add(
                new MovieInfo
                {
                    OriginalTitle = "title1",
                    PosterPath = "/PosterPath1",
                    Overview = "overview1",
                    ReleaseDate = _creationDate,
                    Popularity = 1.0
                });

            movies.Add(
                new MovieInfo
                {
                    OriginalTitle = "title2",
                    PosterPath = "/PosterPath2",
                    Overview = "overview2",
                    ReleaseDate = _creationDate,
                    Popularity = 2.0
                });

            movies.Add(
                new MovieInfo
                {
                    OriginalTitle = "title3",
                    PosterPath = "/PosterPath3",
                    Overview = "overview3",
                    ReleaseDate = _creationDate,
                    Popularity = 3.0
                });

            return movies;
        }
    }
}
