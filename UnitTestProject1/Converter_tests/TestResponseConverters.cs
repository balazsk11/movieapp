﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Converter_tests
{
    using AppWireframe.Converters.MediaResponseConverter;
    using AppWireframe.DataTransferObjects;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
    using System.Collections.ObjectModel;
    using System.Linq;
    using UnitTestProject1.TestData;

    /// <summary>
    /// This class tests the conversion between responses to data transfer objects
    /// </summary>
    [TestClass]
    public class TestResponseConverters
    {
        /// <summary>
        /// Tests conversion between movie responses and media data transfer objects
        /// </summary>
        [TestMethod]
        public void TestMoviesToDataTransferObject()
        {
            //Arrange
            //could be alot better
            ObservableCollection<MediaDataTransferObject> conversionResult;

            //Act
            conversionResult = ResponeToObservableMediaDataTransferObjectConverter
                    .ConvertMoviesToDataTransferCollection(ConverterTestData.MovieInfosTestData);

            //Assert
            Assert.IsNotNull(conversionResult);
            Assert.AreEqual(ConverterTestData.MovieInfosTestData.Count, conversionResult.Count);

            for (int i = 0; i < conversionResult.Count; i++)
            {
                Assert.AreEqual(
                    ConverterTestData.MovieInfosTestData.ElementAt(i).OriginalTitle,
                    conversionResult.ElementAt(i).Heading);

                Assert.AreNotEqual(
                    ConverterTestData.MovieInfosTestData.ElementAt(i).PosterPath,
                    conversionResult.ElementAt(i).PictureUri);

                Assert.AreEqual(
                    ConverterTestData.MovieInfosTestData.ElementAt(i).Overview,
                    conversionResult.ElementAt(i).Overview);

                Assert.AreEqual(
                    ConverterTestData.MovieInfosTestData.ElementAt(i).ReleaseDate,
                    conversionResult.ElementAt(i).ReleaseDate);

                Assert.AreEqual(
                    ConverterTestData.MovieInfosTestData.ElementAt(i).Popularity,
                    conversionResult.ElementAt(i).Rating);
            }
        }

        /// <summary>
        /// Tests conversion between tv show responses and media data transfer objects
        /// </summary>
        [TestMethod]
        public void TestTvShowsToDataTransferObject()
        {
            ObservableCollection<MediaDataTransferObject> conversionResult;

            conversionResult = ResponeToObservableMediaDataTransferObjectConverter
                    .ConvertTvShowsToDataTransferCollection(ConverterTestData.TvShowInfosTestData);

            Assert.IsNotNull(conversionResult);
            Assert.AreEqual(ConverterTestData.TvShowInfosTestData.Count, conversionResult.Count);

            for (int i = 0; i < conversionResult.Count; i++)
            {
                Assert.AreEqual(
                    ConverterTestData.TvShowInfosTestData.ElementAt(i).OriginalName,
                    conversionResult.ElementAt(i).Heading);

                Assert.AreNotEqual(
                    ConverterTestData.TvShowInfosTestData.ElementAt(i).PosterPath,
                    conversionResult.ElementAt(i).PictureUri);

                Assert.AreEqual(
                    ConverterTestData.TvShowInfosTestData.ElementAt(i).Overview,
                    conversionResult.ElementAt(i).Overview);

                Assert.AreEqual(
                    ConverterTestData.TvShowInfosTestData.ElementAt(i).ReleaseDate,
                    conversionResult.ElementAt(i).ReleaseDate);

                Assert.AreEqual(
                    ConverterTestData.TvShowInfosTestData.ElementAt(i).Popularity,
                    conversionResult.ElementAt(i).Rating);
            }
        }

        /// <summary>
        /// Tests conversion between person responses and person data transfer objects
        /// </summary>
        [TestMethod]
        public void PersonsToDataTransferObject()
        {
            ObservableCollection<PersonDataTransferObject> conversionResult;

            conversionResult = ResponeToObservableMediaDataTransferObjectConverter
                    .ConvertPersonsToDataTransferCollection(ConverterTestData.PersonInfosTestData);

            Assert.IsNotNull(conversionResult);
            Assert.AreEqual(ConverterTestData.PersonInfosTestData.Count, conversionResult.Count);

            for (int i = 0; i < conversionResult.Count; i++)
            {
                Assert.AreEqual(
                    ConverterTestData.PersonInfosTestData.ElementAt(i).Name,
                    conversionResult.ElementAt(i).Heading);

                Assert.AreNotEqual(
                    ConverterTestData.PersonInfosTestData.ElementAt(i).PicturePath,
                    conversionResult.ElementAt(i).PictureUri);

                Assert.AreEqual(
                    ConverterTestData.PersonInfosTestData.ElementAt(i).Popularity,
                    conversionResult.ElementAt(i).Popularity);
            }
        }
    }
}