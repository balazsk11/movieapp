﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Converter_tests
{
    using AppWireframe.Converters.PicturePath;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
    using TestHelpers;

    /// <summary>
    /// Contains methods to test picture path conversion
    /// </summary>
    [TestClass]
    public class TestPicturePathConverter
    {
        /// <summary>
        /// Tests picture path conversion
        /// </summary>
        [TestMethod]
        public void TestPictureConverter()
        {
            PicturePathConverter systemUnderTest = new PicturePathConverter();
            string baseUrl = Reflective.GetInstanceField(typeof(PicturePathConverter), systemUnderTest, "baseUrl") as string;
            string picturePath = "/pictureId.png";

            string fullPath = PicturePathConverter.GetPictureFullPath(picturePath);
            string expectedValue = baseUrl + picturePath;

            Assert.IsNotNull(fullPath);
            Assert.AreEqual(expectedValue, fullPath);
        }
    }
}
