﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.TestHelpers
{
    using System;
    using System.Reflection;

    /// <summary>
    /// Contains reflective calls
    /// </summary>
    public static class Reflective
    {
        /// <summary>
        /// returns the value of a an object private field
        /// </summary>
        public static object GetInstanceField(Type type, object instance, string fieldName)
        {
            BindingFlags bindFlags =
                BindingFlags.Instance
                | BindingFlags.Public
                | BindingFlags.NonPublic
                | BindingFlags.Static;

            FieldInfo field = type.GetField(fieldName, bindFlags);
            return field.GetValue(instance);
        }

        /// <summary>
        /// returns the value of a an object private property
        /// </summary>
        public static object GetInstanceProperty(Type type, object instance, string propertyname)
        {
            BindingFlags bindFlags =
                BindingFlags.Instance
                | BindingFlags.Public
                | BindingFlags.NonPublic
                | BindingFlags.Static;

            PropertyInfo property = type.GetProperty(propertyname, bindFlags);
            return property.GetValue(instance);
        }
    }
}
