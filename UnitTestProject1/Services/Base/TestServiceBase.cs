﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Services.Base
{
    using AppWireframe.Services.Base;
    using AppWireframe.Services.Movies;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
    using UnitTestProject1.TestHelpers;

    /// <summary>
    /// test the base class of services
    /// </summary>
    [TestClass]
    public class TestServiceBase
    {
        /// <summary>
        /// Tests api key parameter
        /// </summary>
        [TestMethod]
        public void TestApiKey()
        {
            AbstractGatewayService systemUnderTest = new MoviesService();
            string apiKeyPrefix = Reflective
                .GetInstanceField(typeof(AbstractGatewayService), systemUnderTest, "_apiKeyPrefix") as string;

            string apiKey = Reflective
                .GetInstanceField(typeof(AbstractGatewayService), systemUnderTest, "_apiKey") as string;

            string apikeyParameter = Reflective
                .GetInstanceProperty(typeof(MoviesService), systemUnderTest, "ApiKeyParameter") as string;

            string expectedParameter = apiKeyPrefix + apiKey;

            Assert.IsNotNull(apikeyParameter);
            Assert.AreEqual(expectedParameter, apikeyParameter);
        }
    }
}
