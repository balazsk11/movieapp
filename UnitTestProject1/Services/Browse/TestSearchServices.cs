﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Services.Browse
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Search.Movies;
    using AppWireframe.Services.Search.Persons;
    using AppWireframe.Services.Search.TvShows;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
    using System;

    /// <summary>
    /// This class test search in Movies, tvshows, and people
    /// </summary>
    [TestClass]
    public class TestSearchServices
    {
        private string mediaSeachKeyword = "star";

        private string personSearchKeyword = "john";

        private string failPersonKeyword = "jenn";

        [TestMethod]
        public void TestMovieBrowseResponse()
        {
            IMovieSearchService movieSearchService = new MovieSearchService();

            MovieResponse response = movieSearchService.SearchMoviesAsync(mediaSeachKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        [TestMethod]
        public void TestMovieBrowseresult()
        {
            IMovieSearchService movieSearchService = new MovieSearchService();

            MovieResponse response = movieSearchService.SearchMoviesAsync(mediaSeachKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }

        [TestMethod]
        public void TestTvShowBrowseResponse()
        {
            ITvShowSearchService tvSearchService = new TvShowSearchService();

            TvShowsResponse response = tvSearchService.SearchTvShowsAsync(mediaSeachKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        [TestMethod]
        public void TestTvShowBrowseresult()
        {
            ITvShowSearchService tvSearchService = new TvShowSearchService();

            TvShowsResponse response = tvSearchService.SearchTvShowsAsync(mediaSeachKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }

        [TestMethod]
        public void TestPersonBrowseResponse()
        {
            IPersonSearchService personSearchService = new PersonSearchService();

            PersonResponse response = personSearchService.SearchPersonsAsync(personSearchKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        [TestMethod]
        public void TestPersonBrowseresult()
        {
            IPersonSearchService personSearchService = new PersonSearchService();

            PersonResponse response = personSearchService.SearchPersonsAsync(personSearchKeyword).Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }

        [TestMethod]
        public void TestPersonBrowseFail()
        {
            Assert.ThrowsException<AggregateException>(
                ()=> 
                {
                    IPersonSearchService personSearchService = new PersonSearchService();

                    PersonResponse response = personSearchService.SearchPersonsAsync(failPersonKeyword).Result;
                });
        }
    }
}
