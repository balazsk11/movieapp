﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Services.Browse
{
    using AppWireframe.Beans;
    using AppWireframe.Services.Movies;
    using AppWireframe.Services.Persons;
    using AppWireframe.Services.TvShows;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    /// <summary>
    /// this class tests the browsing services for persons, movies anfd tv shows
    /// </summary>
    [TestClass]
    public class TestBrowseService
    {
        /// <summary>
        /// Tests the movie response from the server
        /// </summary>
        [TestMethod]
        public void TestMovieBrowseResponse()
        {
            IMoviesService moviesService = new MoviesService();

            MovieResponse response = moviesService.GetMoviesAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        /// <summary>
        /// Tests the movie response result from the server
        /// </summary>
        [TestMethod]
        public void TestMovieBrowseresult()
        {
            IMoviesService moviesService = new MoviesService();

            MovieResponse response = moviesService.GetMoviesAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }

        /// <summary>
        /// Tests the tvshow response from the server
        /// </summary>
        [TestMethod]
        public void TestTvShowBrowseResponse()
        {
            ITvShowsService tvService = new TvShowsService();

            TvShowsResponse response = tvService.GetTvShowsAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        /// <summary>
        /// Tests the tvshow response result from the server
        /// </summary>
        [TestMethod]
        public void TestTvShowBrowseresult()
        {
            ITvShowsService tvService = new TvShowsService();

            TvShowsResponse response = tvService.GetTvShowsAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }

        /// <summary>
        /// Tests the person response from the server
        /// </summary>
        [TestMethod]
        public void TestPersonBrowseResponse()
        {
            IPersonsService peopleService = new PersonsService();

            PersonResponse response = peopleService.GetPersonsAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsNotNull(response.Page);
            Assert.IsNotNull(response.TotalPages);
            Assert.IsNotNull(response.TotalResults);
        }

        /// <summary>
        /// Tests the person response result from the server
        /// </summary>
        [TestMethod]
        public void TestPersonBrowseresult()
        {
            IPersonsService peopleService = new PersonsService();

            PersonResponse response = peopleService.GetPersonsAsync().Result;

            Assert.IsNotNull(response.Results);
            Assert.IsTrue(response.Results.Count > 0);
        }
    }
}