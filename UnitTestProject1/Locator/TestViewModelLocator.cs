﻿// --------------------------------------------------------------------------------------------------------------------
// Author: Balázs Koncz
// Date: July of 2016
// Coding is fun! :)
// --------------------------------------------------------------------------------------------------------------------

namespace UnitTestProject1.Locator
{
    using AppWireframe.Locator;
    using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

    /// <summary>
    /// Tests for the view model Locator
    /// </summary>
    [TestClass]
    public class TestViewModelLocator
    {
        /// <summary>
        /// Tests MovieInfo property of the locator
        /// </summary>
        [TestMethod]
        public void TestMovieInfoViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.MovieInfo);
        }

        /// <summary>
        /// Tests TvShowInfo property of the locator
        /// </summary>
        [TestMethod]
        public void TestTvShowInfoViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.TvShowInfo);
        }

        /// <summary>
        /// Tests PersonInfo property of the locator
        /// </summary>
        [TestMethod]
        public void TestPersonInfoViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.PersonInfo);
        }

        /// <summary>
        /// Tests Movies property of the locator
        /// </summary>
        [TestMethod]
        public void TestMoviesViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.Movies);
        }

        /// <summary>
        /// Tests TvShows property of the locator
        /// </summary>
        [TestMethod]
        public void TestTvShowsViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.TvShows);
        }

        /// <summary>
        /// Tests Persons property of the locator
        /// </summary>
        [TestMethod]
        public void TestPersonsViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.Persons);
        }

        /// <summary>
        /// Tests Search property of the locator
        /// </summary>
        [TestMethod]
        public void TestSearchViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.Search);
        }

        /// <summary>
        /// Tests SearchResult property of the locator
        /// </summary>
        [TestMethod]
        public void TestSearchResultViewModel()
        {
            ViewModelLocator systemUnderTest = new ViewModelLocator();

            Assert.IsNotNull(systemUnderTest.SearchResult);
        }
    }
}
